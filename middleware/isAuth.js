const jwt = require('jsonwebtoken')

const isAuth = (token) =>{
    if(!token || token === ''){
        throw new Error("Access denied")
    }
    let decodedToken
    try{
        decodedToken = jwt.verify(token, 'somesupersecretkey')
    }catch(err){
        throw new Error("Access denied")
    }
    if(!decodedToken){
        throw new Error("Access denied")
    }
    console.log(decodedToken.user._id)
    return {userId:decodedToken.user._id}

}

module.exports = {isAuth}