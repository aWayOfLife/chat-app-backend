const { ApolloServer } = require('apollo-server-express')
const express = require('express')
const mongoose = require('mongoose')
const { resolvers } = require('./schema/resolvers')
const {typeDefs} = require('./schema/typeDefs')
const http = require('http')
const cors = require('cors')
require('dotenv/config')



const app = express()
app.use(cors())

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }) => {
        const token = req?.headers?.authorization || '';
        return { token };
    },
    subscriptions: {
    path: '/subscriptions',
    onConnect: (connectionParams, webSocket, context) => {
      console.log('Client connected');
    },
    onDisconnect: (webSocket, context) => {
      console.log('Client disconnected')
    },
    
  }
})

server.applyMiddleware({app})

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

console.log(process.env.DB_CONNECTION)

mongoose.connect(process.env.DB_CONNECTION,{ useNewUrlParser: true, useFindAndModify:true, useUnifiedTopology:true }, () => console.log("connected to DB"))

httpServer.listen(5000, () => {
  console.log(`Server ready at http://localhost:5000/graphql`)
  console.log(`Subscriptions ready at ws://localhost:5000/graphql`)
})
