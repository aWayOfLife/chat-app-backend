const mongoose = require('mongoose')

const UserSchema = mongoose.Schema({
    name:{
        type: String,
        required:true,
        min:3,
        max:255
    },
    email:{
        type:String,
        required:true,
        min:6,
        max:255
    },
    password:{
        type:String,
        require:true,
        min:6,
        max:1024
    },
    conversations:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Conversation'
        }
    ]
})

module.exports = mongoose.model('User', UserSchema)
