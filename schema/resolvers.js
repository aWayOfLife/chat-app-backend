const { PubSub } = require('apollo-server-express')
const User = require('../models/User')
const Message = require('../models/Message')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Conversation = require('../models/Conversation')
const {isAuth} = require('../middleware/isAuth')
const Dataloader = require('dataloader')

const pubSub = new PubSub()
const MESSAGE_CHANNEL = 'MESSAGE_CHANNEL_'
const CONVERSATION_CHANNEL = 'CONVERSATION_CHANNEL_'

const conversationsLoader = new Dataloader((conversationIds) =>{
    return Conversation.find({_id:{$in:conversationIds}})
})

const usersLoader = new Dataloader((userIds)=>{
    return User.find({_id:{$in:userIds}})
})



const getConversation = async (sender,receiver) =>{
  
    const messageSender = await User.findById(sender)
    const messageReceiver = await User.findById(receiver)
    
    const conversation = await Conversation.findOne({members: {"$all":[messageSender,messageReceiver]}})

    if(conversation) return conversation

    const conv = new Conversation({
        members:[messageSender.id,messageReceiver.id]
    })

    const savedConversation = await conv.save()
    await messageSender.conversations.push(savedConversation.id)
    await messageReceiver.conversations.push(savedConversation.id)
    await messageReceiver.save()
    await messageSender.save()
    console.log(savedConversation)
    pubSub.publish(CONVERSATION_CHANNEL+receiver,{
        newConversation:savedConversation
    })
    return savedConversation

}

const resolvers = {
    Subscription:{
        newMessage:{
            subscribe:async (payload, variables)=>{
                const result = pubSub.asyncIterator(MESSAGE_CHANNEL+variables.userId)
                return result
            }
        },
        newConversation:{
            subscribe:async (payload, variables)=>{
                console.log('new conversation logged')
                const result = pubSub.asyncIterator(CONVERSATION_CHANNEL+variables.userId)
                return result
            }
        }
    },
    Query: {
        users:async(parent,args,context) =>{

            console.log('fetching users')
            const currentUser = isAuth(context.token)
            console.log('checked token')
            const users = await User.find({_id:{"$ne":currentUser.userId}})
            return users
        },
        conversations:async(parent, args, context) =>{
            console.log('convs')
            const currentUser = isAuth(context.token)
            console.log('current user',currentUser.userId)
            const conversations= await Conversation.find({members: {"$in":[currentUser.userId]}})
            console.log(conversations)
            return conversations
        },
        messages:async(parent,{conversationId},context) =>{
            console.log('fetching messages')
            const currentUser = isAuth(context.token)
            return await Message.find({'conversation':conversationId})
        },
        loginUser: async(parent,{email, password}) =>{
            console.log("start of login")
            const user = await User.findOne({email:email})
            if(!user) throw new Error('User does not exist')
            const isEqual = await bcrypt.compare(password, user.password)
            if(!isEqual) throw new Error("Incorrect password")
            const token = jwt.sign({user:{_id:user.id, name:user.name, email:user.email}}, 'somesupersecretkey', {
                expiresIn:'1h'
            })
            return {token:token}
        }
    },
    Message:{
        async sender(parent){           
            //return await User.findById(parent.sender)
            const user = await usersLoader.load(parent.sender.toString())
            return user

        },
        async conversation(parent){
            //return await Conversation.findById(parent.conversation)
            const conversation = await conversationsLoader.load(parent.conversation.toString())
            return conversation

        }
    },
    Conversation:{
        async members(parent){
            console.log('parent', parent.members)

            //const members = await usersLoader.loadMany(parent.members.map(member => member.toString()))
            return await parent.members.map(member =>{
                return User.findById(member)
            })
            //return members
        }
    },


    

    Mutation: {
        registerUser: (parent, args) =>{
            console.log(args)
            return User.findOne({email:args.userInput.email}).then(user =>{
                if(user){
                    throw new Error('User exists already')
                }
                 return bcrypt.hash(args.userInput.password, 12)
            })
            .then(hashedPassword =>{
                const user = new User({
                    name: args.userInput.name,
                    email: args.userInput.email,
                    password: hashedPassword
                })
                return user.save()
            })
            .then(result =>{
                const token = jwt.sign({user:{_id:result._doc.id, name:result._doc.name, email:result._doc.email}}, 'somesupersecretkey', {
                expiresIn:'1h'
                })
                return {token:token}
            })
            .catch(err =>{
                throw err
            })    
        },

        sendMessage: async(parent,{receiver, text}, context) =>{
            const currentUser = isAuth(context.token)
            const sender = currentUser.userId
            const conversation = await getConversation(sender, receiver)
            console.log(conversation)
            const message = new Message({
                conversation:conversation.id,
                text:text,
                sender:sender
            })

            const savedMessage =  await message.save();
    
            pubSub.publish(MESSAGE_CHANNEL+receiver,{
               newMessage:savedMessage
            })

            return savedMessage
        }

        



    }
}

module.exports = {resolvers}