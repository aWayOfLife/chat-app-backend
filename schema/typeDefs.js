const {gql} = require('apollo-server-express')

const typeDefs = gql`
    type User{
        _id: ID!
        name:String
        email:String
    }

    type Conversation{
        _id:ID!
        members:[User!]!
    }

    type Message{
        _id: ID!
        conversation: Conversation!
        sender: User!
        createdAt: String!
        updatedAt: String!
        text: String!
    }

    type AuthData{
        token: String!
    }


    input UserInput {
        name:String!
        email: String!
        password: String!
    }

    input MessageInput {
        text: String!
    }


    type Query {
        users:[User!]!
        conversations:[Conversation!]!
        messages(conversationId:String!):[Message!]!
        loginUser(email:String!, password:String!): AuthData!
    }

    type Mutation{
        registerUser(userInput:UserInput):AuthData!
        sendMessage(receiver:String!, text:String!):Conversation!
    }

    #Subscriptions
    type Subscription{
        newMessage(userId:String!):Message!
        newConversation(userId:String!):Conversation!
    }



`

module.exports = {typeDefs}
